# Setup a Kubernetes cluster from scratch

_with [Rancher](http://rancher.com/docs/rancher/v1.6/en/kubernetes/) and [GlusterFS](https://github.com/gluster/gluster-kubernetes)_

## Introduction

I have decided to publish this repository but I have stopped working on it. It will be part of a blog post: I have dropped the whole "from scratch" thing and [stuck with Google Cloud Platform](https://gitlab.com/fhuitelec-learning/poc/google-cloud-platform).

You might find some interesting things but this repository is messy as I did not go on with it. You are now warned.

## Clustering solution

### Cluster manager

I'll take time later to fully document this choice but I'm sticking with [Rancher](http://rancher.com/kubernetes/) to deal with the Kubernetes cluster management.

Here's a few pros:
- Very easy alternative to [kubeadm](https://github.com/hobby-kube/guide) to:
    - provision a Kubernetes cluster
    - provision a **secure** Kubernetes cluster
- Configuration out of the box
- Disaster recovery "out of the box" too
- No need for a costly external load balancer or weird tricks (Hey there `hostPorts` & `hostNetwork` :wave:)
- neat dashboard in addition to the kubernetes one: Host overview, Infrastructure stack, etc.

And a few cons:
- Have to setup an extra - "strong" - node for rancher
- It takes time to the Rancher team to release new Kubernetes versions
- You don't have a full control over how your cluster is provisionned
- You have to hack your way in to get your own reverse-proxy without Rancher LB to take over

### Server topology

To fit a High Availibility setup and GlusterFS needs, we'll provision a 4 nodes cluster. I've chosen [Scaleway](https://www.scaleway.com/) for their - [very](https://www.scaleway.com/pricing/) - low prices, nice UI and features (cheap and easy volume provisioning), and responsive customer service.


Let's sum up:

- 1 node being the **Rancher master** (C2S - 11,99€/month):
	- Only host Rancher, its services and UI
- 3 nodes with an additional 50GB volume (VC1M - 6,99€/month) for **Kubernetes**:
	- Host kubernetes internal services (`etcd`, `kubectld`, `scheduler`, etc.)
	- Host kubernetes daemons (`kubelet`, `proxy`, etc.)
	- Personal pods (i.e. the "computing power")
	- Replicated GlusterFS (`heketi` service, `gluster` daemons)

Total: 32,96€/month

### Private network

I'm using tinc for the [job](https://www.tinc-vpn.org/) ([Nice tutorial on DO](https://www.digitalocean.com/community/tutorials/how-to-install-tinc-and-set-up-a-basic-vpn-on-ubuntu-14-04)).

To bypass learning curve associated to tools like [terraform](https://www.terraform.io/intro/index.html) or [ansible](http://docs.ansible.com/ansible/index.html), I've created in a few hours a basic [CLI](https://gitlab.com/fhuitelec/tinc-subnet-creator) to add a master and nodes.

I strongly suggest using a proper tool to set up your private network and not this CLI.

### Storage

For storage, I've chosen GlusterFS for now as they have a neat [toolkit](https://github.com/gluster/gluster-kubernetes) and other solution are not flexible enough as of today (personal opinion).

## Guide

- [Fire up Kubernetes (Rancher docs)](https://docs.rancher.com/rancher/v1.6/en/kubernetes/)
- Create private network `todo`
- [Storage with GlusterFS](https://gitlab.com/fhuitelec-poc/complete-k8s-cluster/blob/master/storage/README.md)

## Todo

Well, setting up a working Kubernetes was "easy", the hard part remains the same: how do I operate daily my cluster (scalability, high availibility, etc.)?

### Disaster recovery

- [ ] Break 1+ etcd node and [get etcd into recovery mode](https://docs.rancher.com/rancher/v1.6/en/kubernetes/disaster-recovery/)

### Storage use-cases

- [ ] Add a node to the storage topology
