## Traefik

This traefik setup is based on [this article](https://blog.osones.com/en/kubernetes-traefik-and-lets-encrypt-at-scale.html).

⚠️ _This part of the repository is a bit messy and you might want to stick with the above article._
