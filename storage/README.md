# Storage with GlusterFS

Now that we have an up and running Kubernetes cluster. To go on with this part we must have 3 nodes with a 50GB block device on each of them.

As the base of this part, we'll use [this repo](https://github.com/gluster/gluster-kubernetes) and, particularly [the setup guide](https://github.com/gluster/gluster-kubernetes/blob/master/docs/setup-guide.md). Also, there have been a lot of pain points during my first setup and [this article](http://blog.lwolf.org/post/how-i-deployed-glusterfs-cluster-to-kubernetes/) has saved me a lot of time.

---

All 3 nodes are ready:
- On a private network pointing to rancher master (`10.0.0.1`)
- Every node has a 50GB volume attached and formatted
- Every node is part of the rancher/k8s cluster

---

According to [this bit of tuto](https://github.com/gluster/gluster-kubernetes/blob/master/docs/setup-guide.md#2-run-the-deployment-script), I have to setup [`heketi-cli`](https://github.com/heketi/heketi/releases) in the node where I'm going to use `gk-deploy`.

The question(s):
- where should I run `gk-deploy`? (dev machine, rancher-master - need to install `kubectl`)

---

## Run de `gk-deploy`
Run de gk-deploy sur `gluster1`, le k8s-master, pas sur `rancher-master` car les endpoints de k8s sont inaccessible et on a besoin de taper sur l'endpoint de heketi

## Pain points

### Adding device hanging

**Error**
```bash
Adding device /dev/vdb ... # Hanging indefinitely
```

**Solution**
- Barbaric way:

```bash
kubectl get pods|grep glusterfs
# Careful, command below is very destructive
kubectl exec -ti glusterfs-cwp8f pvcreate -ff --metadatasize=128M --dataalignment=256K /dev/vdb
```

- Clean way:

```bash
# On every block volume nodes
apt-get update && apt-get install lvm2 # Install lvm
pvcreate -ff --metadatasize=128M --dataalignment=256K /dev/vdb
```

### Required device-mapper

**Error**
```
Error: Unable to execute command on glusterfs-q11rw:   /usr/sbin/modprobe failed: 1
thin: Required device-mapper target(s) not detected in your kernel.
```
**Solution**
```bash
modprobe dm_thin_pool
```

---

## Create a volume
See [heketi's docs](https://github.com/heketi/heketi/wiki/Create-a-volume) and [gluster-kubernetes' docs](https://github.com/gluster/gluster-kubernetes/blob/master/docs/setup-guide.md#usage-examples).


### Set heketi environment

#### Export heketi endpoint
```bash
export HEKETI_CLI_SERVER=http://$(kubectl get endpoints|grep -v heketi-storage-endpoints|grep heketi|awk '{print $2}')/
```

#### Troubleshooting

The linux binary of `heketi-cli` crashes with an `Error:`. The workaround is to forward the `heketi` pod locally on my machine:

```bash
kubectl port-forward (kubectl get pods|grep heketi|awk '{print $1}') 8080 # Forward port 8080 of the heketi pod locally
export HEKETI_CLI_SERVER=http://localhost:8080 # Set heketi server endpoint
set HEKETI_CLI_SERVER http://localhost:8080 # Fish alternative
```

### Create volume
```bash
heketi-cli volume create --size=30 --replica=3 \
  --persistent-volume \
  --persistent-volume-endpoint=heketi-storage-endpoints | kubectl create -f -
```